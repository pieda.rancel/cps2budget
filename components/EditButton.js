import React,{useState} from 'react'
import {Tooltip, IconButton} from '@material-ui/core'
import EditIcon from '@material-ui/icons/Edit';


export default function EditButton({
	canEdit, 
	setCanEdit,
	setDescription,
	setName,
	setType,
	setAmount,
	setCategoryId,
	name,
	type,
	categoryId,
	amount,
	description
}) {

	const handleEdit = () => {
		setCanEdit(!canEdit)
		setDescription(description)
		setName(name)
		setType(type)
		setAmount(amount)
		setCategoryId(categoryId)
	}

	return (
      <Tooltip title="Edit" placement="right">
        <IconButton onClick = {handleEdit} aria-label="edit" type="submit">
          <EditIcon title="Edit"/>
        </IconButton>
      </Tooltip>
	)
}