import React, {useState} from 'react'
import {Table} from 'react-bootstrap'
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import {TextField} from '@material-ui/core'
import EditButton from './EditButton'
import TableRow from './TableRow'

export default function BootstrapTable({records, setRecords}) {
    
  return (
    <>
    <style type="text/css">
      {`
        table {
          background-color: whitesmoke;
        }

        th {
          background-color: gray;
        }

        thead {
          color: white;
          text-align: center;          
        }

        .actionDiv {
          display: flex;
          align-items: center;
          justify-content: space-around;
        }

        .tableData {
          text-align: center;
          vertical-align: middle !important;
        }

        `}
    </style>


      <Table responsive hover>
            <thead>
              <tr>
                <th>Action</th>
                <th>Name</th>
                <th>Category</th>
                <th>Amount</th>
                <th>Type</th>
                <th>Description</th>
              </tr>
            </thead>

            <tbody>

            { records.map(record => {
              return (
              <TableRow key = {record._id} record = {record} setRecords = {setRecords} />
                )
            })
            }

            </tbody>
      </Table>
    </>
  )
}