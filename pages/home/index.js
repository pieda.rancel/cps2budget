import React, {useState, useEffect, useContext} from 'react'
import NavBar from '../../components/NavBar'
import {Col, Row, Container, Card} from 'react-bootstrap'
import { makeStyles } from "@material-ui/core/styles"
import TextField from "@material-ui/core/TextField"
import MenuItem from "@material-ui/core/MenuItem"
import Button from '@material-ui/core/Button'
import {useStyles} from '../../styleAPI'
import MyTable from '../../components/MyTable'
import BootstrapTable from '../../components/BootstrapTable'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip';
import AddCircleIcon from '@material-ui/icons/AddCircle'
import AddDialog from '../../components/AddDialog'
import UserContext from '../../UserContext'
import swal from 'sweetalert2'



export default function index() {
	    const classes = useStyles();
	    const [open, setOpen] = useState(false);
	    const [categories, setCategories] = useState([])
	    const [name, setName] = useState('')
	    const [categoryId, setCategoryId] = useState('')
	    const [type, setType] = useState('')
	    const [amount, setAmount] = useState('')
	    const [description, setDescription] = useState('')
	    const [previousUserBalance, setPreviousUserBalance] = useState('')
	    const [records, setRecords] = useState([])


	    const {user, balance, setBalance} = useContext(UserContext)

	    const handleAddCategory = (e) => {
    		setOpen(true);
  		};

  		const submitRecord = (e) => {
  			if(name !== "" && categoryId !== "" && type !== "" && amount !== ""){
  			e.preventDefault()
  			const newBalance = () => {
  				if(type === "Expense"){
  					return +balance - +amount
  				} else {
  					return +balance + +amount
  				}
  			}

  			const balanceToServer = newBalance()

  			fetch(`${process.env.NEXT_PUBLIC_API_URL}/records/`, {
  				method: "POST",
  				headers: {
  					"Content-Type": "application/json",
  					Authorization: `Bearer ${localStorage.getItem('token')}`
  				},
  				body: JSON.stringify({
  					name,
  					categoryId,
  					type,
  					amount,
  					description
  				})
  			})
  			.then(res => res.json())
  			.then(data => {
  				if(data === false){
  					swal.fire('Data Error', 'Something went wrong when fetching the data', 'error')
  				} else {
  					fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${data}`, {
  						method: "PATCH",
  						headers: {
  							"Content-Type": "application/json",
  							Authorization: `Bearer ${localStorage.getItem('token')}`
  						},
  						body: JSON.stringify({
  							balance: balanceToServer
  						})
  					})
  					.then(res => res.json())
  					.then(data => {
  						if(data === false){
  							swal.fire('Data Error', 'Something went wrong when fetching the data', 'error')
  						} else {
  							setRecords(data.records.filter(record => record.isActive === true))
  							setBalance(data.balance)
  							swal.fire('Congratulations', 'New record successfully added')
  						}
  					})
  				}
  			})
  		}
  		}

  		const handleOnChange = (e) => {
  			if(e.target.name === "recordName"){
  				setName(e.target.value)
  			} else if (e.target.name === "categoryId") {
  				setCategoryId(e.target.value)
  			} else if (e.target.name === "categoryType"){
  				setType(e.target.value)
  			} else if (e.target.name === "amount") {
  				setAmount(e.target.value)
  			} else if (e.target.name === "description") {
  				setDescription(e.target.value)
  			}
  		}

  		useEffect(()=> {
  			fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories`, {
  				headers: {
  					Authorization: `Bearer ${localStorage.getItem('token')}`
  				}
  			})
  			.then(res => res.json())
  			.then(data => {
  				setCategories(data)
  				fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${user.id}`,{
  					headers: {
  						Authorization: `Bearer ${localStorage.getItem('token')}`
  					}
  				})
  				.then(res => res.json())
  				.then(data => {
  					setRecords(data.records.filter(record => record.isActive === true))
  				})
  			})


  		}, [])


	return (
		<>
		<style type="text/css">
			{`
				.bg-whitesmoke {
					background-color: whitesmoke;
				}

				.shadow {
					box-shadow: -23px 22px 5px 17px rgba(0,0,0,0.75);
				}

				.h-80 {
					height: 80%;
					overflow-y: auto;
					background-color: whitesmoke;
				}
				`}
		</style>
		<NavBar />
		<Container fluid>
			<Row style={{backgroundImage: "url('/fortuneBG.jpg')", backgroundRepeat: "no-repeat", backgroundSize: "cover", backgroundPosition: "center"}}>
				<Col className={classes.rootPages} lg={4}>
					<Col className="h-75 mt-5" xs sm={{span: 10, offset: 1}} style={{padding: "0 0 !important"}}>
						<Card className="h-100 bg-whitesmoke shadow">
									<Card.Body>
										<h3 className= "text-center">CREATE RECORD</h3>				
										<form onSubmit = { submitRecord } className={classes.rootFormRecord} noValidate autoComplete="off">
											<Row>
												<Col xs={12}>
												<Row className="align-items-center justify-content-center">
													 <TextField name = "recordName" label="Record Name" onChange = {handleOnChange}/>
												</Row>
												</Col>
												<Col xs={12}>
												<Row className="align-items-center justify-content-center">
													<Col xs={10} className="pr-0">
													<TextField
													value={categoryId}
													name="categoryId"
													onChange = {handleOnChange}
													select
													label="Category">
													 {	categories.length > 0 
														? categories.map(category => {
														return (
																<MenuItem key = {category._id} value={category._id}>{category.name}</MenuItem>
															)
													}) : 
														<MenuItem>No Categories Listed Yet</MenuItem>
													}
													</TextField>
													</Col>
													<Col xs={2} className="pl-0 pb-0">
															<Tooltip title="Add Category" placement="right">
                        										<IconButton onClick = {handleAddCategory} aria-label="edit" color="primary">
                        											<AddCircleIcon />
                        										</IconButton>
                        									</Tooltip>
													</Col>
												</Row>
												</Col>
												<Col xs={12}>
												<Row className="align-items-center justify-content-center">
													<TextField
															  name = "categoryType"
															  onChange = {handleOnChange}
													          select
													          value = {type}
													          label="Category Type">
													<MenuItem value="Expense">Expense</MenuItem>
													<MenuItem value="Income">Income</MenuItem>
													</TextField>
												</Row>
												</Col>
												<Col xs={12}>
												<Row className="align-items-center justify-content-center">
													<TextField onChange = {handleOnChange} name="amount" label="Amount" type="number" />
												</Row>
												</Col>
												<Col xs={12}>
												<Row className="align-items-center justify-content-center">
													<TextField 
													onChange = {handleOnChange}
													name = "description"
													label="Description" 
													variant="outlined"
													multiline
													rows={4}
													/>
												</Row>
												</Col>
												<Col xs={{span: 6, offset: 3}}>
													<Button className="col-12" variant="contained" color="primary" type="submit">Submit</Button>
												</Col>
											</Row>

										</form>
									</Card.Body>
						</Card>
					</Col>
				</Col>
				<Col className={classes.rootPages} lg={8}>
					<Row className="h-80 border border-dark mt-5 align-items-start w-100 mx-auto justify-content-center">
						<BootstrapTable records = {records} setRecords = {setRecords}/>
					</Row>
				</Col>
			</Row>
		</Container>


		<AddDialog setCategories = {setCategories} open={open} setOpen = {setOpen}/>
		</>
	)
}
