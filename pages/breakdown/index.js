import React, {useState, useEffect, useContext} from 'react'
import NavBar from '../../components/NavBar'
import {Container, Col, Row} from 'react-bootstrap' 
import PolarChart from '../../components/PolarChart'
import DividerMaterial from '../../components/DividerMaterial'
import UserContext from '../../UserContext'

export default function index() {
	const {user} = useContext(UserContext)
	const [iRLengths, setIRLengths] = useState(0)
	const [eRLengths, setERLengths] = useState(0)
	const [eRAmounts, setERAmounts] = useState(0)
	const [iRAmounts, setIRAmounts] = useState(0)
	const [records, setRecords] = useState([])
	const [recordNames, setRecordNames] = useState([])
	const [activeExpenses, setActiveExpenses] = useState([])
	const [activeExpensesNames, setActiveExpensesNames] = useState([])
	const [activeIncomes, setActiveIncomes] = useState([])
	const [activeIncomesNames, setActiveIncomesNames] = useState([])

	const [lengthsClicked, setLengthsClicked] = useState(true)
	const [amountsClicked, setAmountsClicked] = useState(false)
	const [allActiveClicked, setAllActiveClicked] = useState(false)
	const [allActiveIncomesClicked, setAllActiveIncomesClicked] = useState(false)
	const [allActiveExpensesClicked, setAllActiveExpensesClicked] = useState(false)

	const handleInput = (value) => {

	}

	useEffect(()=> {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${user.id}`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			const filterIncomeRecords = data.records.filter(record => record.isActive === true && record.type === 'Income')
			const filterExpenseRecords = data.records.filter(record => record.isActive === true && record.type === 'Expense')

			const filteredIRLength = filterIncomeRecords.length
			const filteredERLength = filterExpenseRecords.length

			const filteredIRAmounts = filterIncomeRecords.map(record => record.amount).reduce((accumulatedValue, currentValue) => {return +accumulatedValue + +currentValue}, 0)
			const filteredERAmounts = filterExpenseRecords.map(record => record.amount).reduce((accumulatedValue, currentValue) => {return +accumulatedValue + +currentValue}, 0)

			const filteredIRNames = filterIncomeRecords.map(record => record.name)
			const filteredERNames = filterExpenseRecords.map(record => record.name)

			const activeNames = data.records.filter(record =>  record.isActive === true).map(record => {
				return record.name
			})
			const activeRecords = data.records.filter(record => record.isActive === true).map(record => {
				return record.amount
			})


			const incomes = filterIncomeRecords.map(income => {
				return income.amount
			})
			const expenses = filterExpenseRecords.map(expense => {
				return expense.amount
			})

			setActiveExpenses(expenses)
			setActiveIncomes(incomes)

			setRecords(activeRecords)
			setRecordNames(activeNames)

			setIRLengths(filteredIRLength)
			setERLengths(filteredERLength)
			setIRAmounts(filteredIRAmounts)
			setERAmounts(filteredERAmounts)

			setActiveExpensesNames(filteredERNames)
			setActiveIncomesNames(filteredIRNames)
		})
	}, [])


	return (
		<>
		<style>
			{`
				.vh-90 {
				height: 90vh;
				}
				.bgBreakdown {
				background: url('/breakdown.jpg');
				background-position: left;
				background-size: cover;
				}
				.whitesmoke {
					background: radial-gradient(circle, rgba(222,218,218,0.7539390756302521) 0%, rgba(230,227,227,0.7763480392156863) 100%); 
				}
				.h-60 {
					height: 60%;
				}
				`}
		</style>


		<NavBar />
		<Container className = "vh-100 bgBreakdown" fluid>
			<Row className="vh-90">
				<Col sm={3} className="h-50 mt-2">
					<Col className="h-100 d-flex align-items-center" sm={12}>
					<DividerMaterial
					setAllActiveExpensesClicked = {setAllActiveExpensesClicked}
					setAllActiveIncomesClicked = {setAllActiveIncomesClicked}
					setAllActiveClicked = {setAllActiveClicked}
					setLengthsClicked = {setLengthsClicked}
					setAmountsClicked = {setAmountsClicked}
					lengthsClicked = {lengthsClicked}
					amountsClicked = {amountsClicked}
					allActiveClicked = {allActiveClicked}
					allActiveIncomesClicked = {allActiveIncomesClicked}
					allActiveExpensesClicked = {allActiveExpensesClicked}
					/>
					</Col>
				</Col>
				<Col sm={8} className="h-60 d-flex p-5 flex-column justify-content-start align-items-start">
					<Col sm={12} className="whitesmoke d-flex flex-column align-items-center justify-content-start mh-50">
						<h1 className="display-4">Breakdown of Budget</h1>
						<PolarChart 
						allActiveClicked = {allActiveClicked}
						lengthsClicked = {lengthsClicked}
						amountsClicked = {amountsClicked}
						recordNames = {recordNames}
						activeExpensesNames = {activeExpensesNames}
						activeIncomesNames = {activeIncomesNames}
						activeExpenses = {activeExpenses}
						activeIncomes = {activeIncomes}
						iRLengths = {iRLengths}
						eRLengths = {eRLengths}
						iRAmounts = {iRAmounts}
						eRAmounts = {eRAmounts}
						records = {records}
						allActiveIncomesClicked = {allActiveIncomesClicked}
						allActiveExpensesClicked = {allActiveExpensesClicked}
						className="h-100"/>
					</Col>
				</Col>
			</Row>
		</Container>
		</>
	)
}


